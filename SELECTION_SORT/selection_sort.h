/*******************
* FILE : selection_sort.h
* Author : Shaswata
* Date : 29/10/2015
********************
* Problem : Implement Selection Sort
********************/

#include "../DISPLAY/print_array.h"

#ifndef SELECTION_SORT
#define SELECTION_SORT

/** FUNCTION SELECTION_SORT
* 	@params
*		array : pointer to main array
*		length : length of array to be sorted
*		debug : if 'Y' or 'y', then print debug info
*/

void selectionSort(int *array, int length, char debug) {
  int index_of_min = 0, x, y;
  for( x = 0; x < length; x++ ) {
    index_of_min = x;
		for( y = x; y < length; y++ ) {
			if( array[index_of_min] > array[y] )
				index_of_min = y;
		}
		int temp = array[x];
		array[x] = array[index_of_min];
		array[index_of_min] = temp;

    // DEBUG
		if(debug == 'Y' || debug == 'y') {
			printf("Pass %d : ", x+1);
			printArray(array, length);
		}

	}
}

#endif
