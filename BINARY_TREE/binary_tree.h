/*******************
* FILE : binary_tree.h
* Author : Argha
* Date : 19/11/2015
********************
* Problem : Implement Binary Tree
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>

typedef struct BTreeNode{
	struct BTreeNode *leftchild;
	int data;
	struct BTreeNode *rightchild;
} BTNode;

/** FUNCTION INSERT NODE
* 	@params
*		**sr : pointer to B TREE'S Root Node
*		num : data to insert
*/
void insert(BTNode **sr, int num){
	if( *sr == NULL ){
		*sr = ( BTNode * )malloc(sizeof(BTNode));
		(*sr)->leftchild=NULL;
		(*sr)->data=num;
		(*sr)->rightchild=NULL;
		return;
	}
	else {
		/*search the node to which new node will be attached*/
		/*if new data is less,travesre to left*/
		if(num<(*sr)->data)
			insert(&((*sr)->leftchild),num);
		else
			/*else traverse to right*/
			insert(&((*sr)->rightchild),num);
	}
	return;
}

/** FUNCTION INORDER TRAVERSAL
* 	@params
*		**sr : pointer to B TREE'S Root Node
/*traverse a binary search tree in a LDR(Left-Data-Right)fashion*/
void inorder(BTNode *sr){
	if(sr!=NULL) {
		inorder(sr->leftchild);
		/*print the data of the node whose leftchild is NULL or the path has already been traversed*/
		printf("\t%d",sr->data);
		inorder(sr->rightchild);
	}
	else
		return;
}

/** FUNCTION PREORDER TRAVERSAL
* 	@params
*		**sr : pointer to B TREE'S Root Node
/*traverse a binary search tree in a DLR(Data-Left-Right) fashion*/
void preorder(BTNode *sr){
	if(sr!=NULL) {
		/*print the data of a node*/
		printf("\t%d",sr->data);
		/*traverse till leftchild is not NULL*/
		preorder(sr->leftchild);
		/*traverse till rightchild is not NULL*/
		preorder(sr->rightchild);}
	else
		return;
}

/** FUNCTION POSTORDER TRAVERSAL
* 	@params
*		**sr : pointer to B TREE'S Root Node
/*traverse a binary search tree in a LRD(Left-Right-Data) fashion*/
void postorder(BTNode *sr){
	if(sr!=NULL){
		postorder(sr->leftchild);
		postorder(sr->rightchild);
		printf("\t%d",sr->data);}
	else
		return;
}

// EOF
