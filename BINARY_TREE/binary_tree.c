/*******************
* FILE : binary_tree.c
* Author : Argha
* Date : 19/11/2015
********************
* Problem : Implement Binary Tree
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "binary_tree.h"

/** MAIN FUNCTIOn **/
main() {
	BTNode *root;

	int req, i = 1, num;

	root = NULL;
	printf("\nNumber of Nodes to be inserted in tree : ");
	scanf("%d", &req);
	while( i++ <= req ) {
		printf("\nEnter Data : ");
		scanf("%d", &num);
		insert(&root, num);
	}

	printf("\nIn-order Traversal : ");
	inorder(root);
	printf("\nPre-order Traversal : ");
	preorder(root);
	printf("\nPost-order Traversal : ");
	postorder(root);
	printf("\n");
}
