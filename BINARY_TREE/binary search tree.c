/*Binary Search Tree*/
#include <stdio.h>
#include <stdlib.h>

typedef struct BTreeNode{
	struct BTreeNode *leftchild;
	int data;
	struct BTreeNode *rightchild;
} BTNode;

void insert(BTNode **,int);
void inorder(BTNode *);
void preorder(BTNode *);
void postorder(BTNode *);

main() {
	BTNode *root;
	int req, i = 1, num;

	root = NULL;
	printf("\n Specify the number of items to be inserted:");
	scanf("%d", &req);
	while( i++ <= req ) {
		printf("\nEnter the data:");
		scanf("%d", &num);
		insert(&root, num);
	}

	printf("\n In-order Traversal:");
	inorder(root);
	printf("\n Pre-order Traversal:");
	preorder(root);
	printf("\n Post-order Traversal");
	postorder(root);
	printf("\n");
}

/*insert a new node in a binary search tree*/
void insert(BTNode **sr, int num){
	if( *sr == NULL ){
		*sr = ( BTNode * )malloc(sizeof(BTNode));
		(*sr)->leftchild=NULL;
		(*sr)->data=num;
		(*sr)->rightchild=NULL;
		return;
	}
	else {
		/*search the node to which new node will be attached*/
		/*if new data is less,travesre to left*/
		if(num<(*sr)->data)
			insert(&((*sr)->leftchild),num);
		else
			/*else traverse to right*/
			insert(&((*sr)->rightchild),num);
	}
	return;
}

/*traverse a binary search tree in a LDR(Left-Data-Right)fashion*/
void inorder(BTNode *sr){
	if(sr!=NULL) {
		inorder(sr->leftchild);
		/*print the data of the node whose leftchild is NULL or the path has already been traversed*/
		printf("\t%d",sr->data);
		inorder(sr->rightchild);
	}
	else
		return;
}

/*traverse a binary search tree in a DLR(Data-Left-Right) fashion*/
void preorder(BTNode *sr){
	if(sr!=NULL) {
		/*print the data of a node*/
		printf("\t%d",sr->data);
		/*traverse till leftchild is not NULL*/
		preorder(sr->leftchild);
		/*traverse till rightchild is not NULL*/
		preorder(sr->rightchild);}
	else
		return;
}
/*traverse a binary search tree in a LRD(Left-Right-Data) fashion*/
void postorder(BTNode *sr){
	if(sr!=NULL){
		postorder(sr->leftchild);
		postorder(sr->rightchild);
		printf("\t%d",sr->data);}
	else
		return;
}
