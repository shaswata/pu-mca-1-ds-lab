/*******************
* FILE : insertion_sort.h
* Author : Shaswata
* Date : 11/09/2015
********************
* Problem : Implement Insertion Sort
********************/

#include "../DISPLAY/print_array.h"


#ifndef INSERTION_SORT
#define INSERTION_SORT

/** FUNCTION INSERTION_SORT
* 	@params
*		array : pointer to main array
*		length : length of array to be sorted
*		debug : if 'Y' or 'y', then print debug info
*/

void insertionSort(int *array, int length, char debug) {
	int i = 1, j, temp;
	for( j = 2; j < length; j++) {
		temp = array[j];
		// Insert array[j] into the sorted sequence array[1...j-1]
		i = j - 1;
		while( i > 0 && array[i] > temp) {
			array[i+1] = array[i];
			i--;
		}
		array[i+1] = temp;
		// DEBUG
		if(debug == 'Y' || debug == 'y') {
			printf("Pass %d : ", j-1);
			printArray(array, length);
		}
	}
}

#endif
