/*******************
* FILE : binary_tree.h
* Author : Argha
* Date : 19/11/2015
********************
* Problem : Implement Binary Tree
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>

typedef struct BSTNode{
	struct BSTNode *leftchild;
	int data;
	struct BSTNode *rightchild;
} BSTNode;

/** FUNCTION INSERT NODE
* 	@params
*		**sr : pointer to B TREE'S Root Node
*		num : data to insert
*/
void insert(BSTNode **sr, int num){
	if( *sr == NULL ){
		*sr = ( BSTNode * )malloc(sizeof(BSTNode));
		(*sr)->leftchild=NULL;
		(*sr)->data=num;
		(*sr)->rightchild=NULL;
		return;
	}
	else {
		/*search the node to which new node will be attached*/
		/*if new data is less,travesre to left*/
		if(num<(*sr)->data)
			insert(&((*sr)->leftchild),num);
		else
			/*else traverse to right*/
			insert(&((*sr)->rightchild),num);
	}
	return;
}

/** FUNCTION INORDER TRAVERSAL
* 	@params
*		**sr : pointer to B TREE'S Root Node
/*traverse a binary search tree in a LDR(Left-Data-Right)fashion*/
void traverseBST(BSTNode *sr){
	if(sr!=NULL) {
		traverseBST(sr->leftchild);
		/*print the data of the node whose leftchild is NULL or the path has already been traversed*/
		printf("\t%d",sr->data);
		traverseBST(sr->rightchild);
	}
	else
		return;
}

/** FUNCTION SEARCH
* 	@params
*		**sr : pointer to B TREE'S Root Node
*		key : key to search in BST
/*traverse a binary search tree in a LDR(Left-Data-Right)fashion*/
int search(BSTNode *sr, int key){
	if(sr == NULL) {
		return 0;
	}
	if(key<sr->data)
		search(sr->leftchild, key);
	else if(key>sr->data)
		search(sr->rightchild, key);
	else
		return 1;
}

// EOF
