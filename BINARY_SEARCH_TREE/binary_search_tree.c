/*******************
* FILE : binary_tree.c
* Author : Argha
* Date : 19/11/2015
********************
* Problem : Implement Binary Tree
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "binary_search_tree.h"

/** MAIN FUNCTIOn **/
main() {
	BSTNode *root;

	int req, i = 1, num;
	char choice;
	root = NULL;

	do {
    printf("\nChoose Operation :\n**********************************************");
    printf("\n1. Traverse BST.\n2. Insert Node at BST.\n3. Search BST.");
    printf("\n0. Exit Program.\n\n");
    scanf(" %c", &choice);
    printf("\n\n");
    switch(choice) {
      case '1' :
        // Traverse List
        printf("Traversing BST : \n");
        traverseBST(root);
        break;
      case '2' :
        // Insert node
        printf("\nEnter Number to store in node : ");
        scanf("%d", &num);
        insert(&root, num);
        printf("\nNode Inserted.\n\n");
        break;
      case '3' :
        // Search Node
        printf("\nEnter Key to search in BST : ");
        scanf("%d", &num);
        if(search(root, num))
          printf("\nElement FOUND in BST.\n\n");
        else
          printf("\nElement NOT FOUND in BST\n\n");
        break;
      case '0' :
        // Exit program
        printf("\n\n***** THANK YOU *****\n\n");
        return EXIT_SUCCESS;
        break;
      default :
        printf("\n\aWRONG CHOICE! TRY AGAIN\n");
        break;
    }
  }while(1);

	printf("\n");
	return EXIT_SUCCESS;
}
