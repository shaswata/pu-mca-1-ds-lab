/*******************
* FILE : linear_search.h
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement linear searching technique
********************/

#ifndef LINEAR_SEARCH
#define LINEAR_SEARCH


/* FUNCTION LINEAR_SEARCH
* 	@params
*		 array : pointer to main array
*		 key 	: KEY to search in the array
*		 length : length of array to be sorted
*/
int linearSearch(int *array, int key, int length) {
  int i = 0;
	printf("Searching for %d in array : \n", key);
	for(; i < length; i++) {
		if(array[i] == key)
			return i+1;
	}
	return 0;
}

#endif
