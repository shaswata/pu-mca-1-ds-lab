/*******************
* FILE : dequeue.c
* Author : Shaswata
* Date : 21/11/2015
********************
* Problem : Print an array
********************/


/** ALL INCLUDES HERE **/
#include <stdio.h>
#include <stdlib.h>
#include "dequeue.h"

/* MAIN FUNCTION */
int main() {
  int data, ch;
  createSentinels();
  while (1) {
    printf("1. Enqueue at front\n2. Enqueue at rear\n");
    printf("3. Dequeue at front\n4. Dequeue at rear\n");
    printf("5. Display\n6. Exit\n");
    printf("Enter your choice:");
    scanf("%d", &ch);
    switch (ch) {
      case 1:
        printf("Enter the data to insert:");
        scanf("%d", &data);
        enqueueAtFront(data);
        break;
      case 2:
        printf("Enter ur data to insert:");
        scanf("%d", &data);
        enqueueAtRear(data);
      break;
      case 3:
        dequeueAtFront();
        break;
      case 4:
        dequeueAtRear();
        break;
      case 5:
        display();
        break;
      case 6:
        return EXIT_SUCCESS;
      default:
        printf("Pls. enter correct option\n");
      break;
    }
  }
  return EXIT_SUCCESS;
}
