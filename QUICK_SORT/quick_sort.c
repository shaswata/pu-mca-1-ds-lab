/*******************
* FILE : quick_sort.c
* Author : Shaswata
* Date : 19/11/2015
********************
* Problem : Implement Quick Sort
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "../DISPLAY/print_array.h"
#include "quick_sort.h"
// #include <conio.h>

/* MAIN FUNCTION*/
int main() {
	int *array, i = 0, length;
	// clrscr();
	printf("Enter Required Lenght for the array : ");
	scanf("%d", &length);

	// Allocate memory for array
	array = (int *) malloc(sizeof(int) * length);
	if(array == NULL) {
		printf("\nError : Memory Not Allocated!\n");
		// exit(1);
		return EXIT_FAILURE;
	}
	for(; i < length; i++ ) {
		printf("\nEnter element %d of %d : ", i+1, length);
		scanf("%d", &array[i]);
	}
	printf("\nOriginal Array : \n");
	printArray(array, length);
	quickSort(array, 0, length - 1);
	printf("\n\nSorted Array : \n");
	printArray(array, length);
	// getch();
	return EXIT_SUCCESS;
}

// EOF
