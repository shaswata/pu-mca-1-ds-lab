/*******************
* FILE : quick_sort.h
* Author : Shaswata
* Date : 19/11/2015
********************
* Problem : Implement Quick Sort
********************/

#include "../DISPLAY/print_array.h"


#ifndef QUICK_SORT
#define QUICK_SORT

/** FUNCTION SWAP
* 	@params
*		*a : pointer to one variable to swap
*   *b : pointer to second variable to swap
*/
void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

/** FUNCTION PARTITION
* 	@params
*		arr : pointer to main array
*/
/* This function takes last element as pivot, places the pivot element at its
   correct position in sorted array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right of pivot */
int partition (int arr[], int l, int h) {
    int x, i, j;
    x = arr[h];    // pivot
    i = (l - 1);  // Index of smaller element

    for (j = l; j <= h- 1; j++) {
        // If current element is smaller than or equal to pivot
        if (arr[j] <= x) {
            i++;    // increment index of smaller element
            swap(&arr[i], &arr[j]);  // Swap current element with index
        }
    }
    swap(&arr[i + 1], &arr[h]);
    return (i + 1);
}

/** FUNCTION PARTITION
* 	@params
*		arr : pointer to main array
*   l  : Starting index
*   h  : Ending index
*/
void quickSort(int arr[], int l, int h) {
    if (l < h) {
        int p = partition(arr, l, h); /* Partitioning index */
        quickSort(arr, l, p - 1);
        quickSort(arr, p + 1, h);
    }
}

#endif

// EOF

/**
quicksort(A, lo, hi)
  if lo < hi
    p = partition(A, lo, hi)
    quicksort(A, lo, p - 1)
    quicksort(A, p + 1, hi)

partition(A, lo, hi)
    pivot = A[hi]
    i = lo #place for swapping
    for j = lo to hi - 1
        if A[j] <= pivot
            swap A[i] with A[j]
            i = i + 1
    swap A[i] with A[hi]
    return i
*/
