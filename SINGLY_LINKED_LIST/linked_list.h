/*******************
* FILE : linked_list.h
* Author : Shaswata
* Date : 02/11/2015
********************
* Problem : Implement Singly Linked List
********************/


#ifndef S_LL
#define S_LL

#include <errno.h>

typedef struct node {
	int data;
	struct node *next;
} Node;

Node * createNode(int data) {
	Node *new_node = (Node *) malloc(sizeof(Node));
	if( new_node == NULL ) {
		fprintf(stderr, "\n\n\aToo many nodes! Memory could not be allocated!\n\n");
	}
	new_node->data = data;
	new_node->next = NULL;
	return new_node;
}

char listIsEmpty(Node *start) {
	if( start == NULL )
		return 1;
	return 0;
}

void traverse(Node *start) {
	Node *temp = start;
	if(listIsEmpty(start)) {
		printf("\n\nList is empty.\n\n");
		return;
	}
	while(temp != NULL) {
		printf("%d, ", temp->data);
		temp = temp->next;
	}
}

int countNodes(Node *start) {
	int count = 0;
	Node *temp = start;
	while(temp != NULL) {
		// printf("%d, ", temp->data);
		temp = temp->next;
		count++;
	}
	return count;
}

char insertBeginning(Node **start, int data) {
	Node *new_node = createNode(data);
	if(new_node != NULL) {
		new_node->next = *start;
		*start = new_node;
		return 1;
	}
	return 0;
}

char insertEnd(Node **start, int data) {
	Node *temp, *new_node, *save;
	new_node = createNode(data);
	if(new_node == NULL)
		return 0;
	if(*start == NULL) {
		*start = new_node;
		return 1;
	}
	temp = *start;
	// Traverse till the end
	while(temp->next != NULL)
		temp = temp->next;
	temp->next = new_node;
	return 1;
}

char insertAtPosition(Node **start, int data, int position) {
	Node *temp, *new_node, *save;
	new_node = createNode(data);
	if(new_node == NULL)
		return 0;
	if(position == 1) {
		new_node->next = *start;
		*start = new_node;
		return 1;
	}
	if(position > countNodes(*start) + 1 || position == 0) {
		printf("\nError : Invalid position given.\n");
		return 0;
	}
	temp = *start;
	// Traverse till the position
	for(; position > 2 ; position-- )
		temp = temp->next;
	new_node->next = temp->next;
	temp->next = new_node;
	return 1;
}

char deleteBeginning(Node **start) {
	Node *temp = *start;
	if(*start == NULL) {
		printf("\nError : Nothing to delete\n");
		return 0;
	}
	*start = temp->next;
	free(temp);
	return 1;
}

char deleteEnd(Node **start) {
	Node *save, *temp = *start;
	if(*start == NULL) {
		printf("\nError : Nothing to delete\n");
		return 0;
	}
	if( temp->next == NULL ) {
		*start = NULL;
		return 1;
	}
	while( temp->next != NULL ) {
		save = temp;
		temp = temp->next;
	}
	save->next = NULL;
	free(temp);
	return 1;
}

char deleteAtPosition(Node **start, int position) {
	Node *temp, *save;
	temp = *start;
	if(*start == NULL) {
		printf("\nError : Nothing to delete\n");
		return 0;
	}
	if(position == 1) {
		*start = temp->next;
		return 1;
	}
	if(position > countNodes(*start) || position == 0) {
		printf("\nError : Invalid position given.\n");
		return 0;
	}
	// Traverse till the position
	for(; position > 1 ; position-- ){
		save = temp;
		temp = temp->next;
	}
	save->next = temp->next;
	free(temp);
	return 1;
}

#endif

// EOF
