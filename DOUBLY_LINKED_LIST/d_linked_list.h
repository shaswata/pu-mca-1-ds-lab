/*******************
* FILE : linked_list.h
* Author : Shaswata
* Date : 18/11/2015
********************
* Problem : Implement Doubly Linked List
********************/


#ifndef D_LL
#define D_LL

#include <errno.h>

typedef struct node {
	int data;
	struct node *next;
	struct node *prev;
} DNode;

DNode * createNode(int data) {
	DNode *new_node = (DNode *) malloc(sizeof(DNode));
	if( new_node == NULL ) {
		fprintf(stderr, "\n\n\aToo many nodes! Memory could not be allocated!\n\n");
	}
	new_node->data = data;
	new_node->next = NULL;
	new_node->prev = NULL;
	return new_node;
}

char listIsEmpty(DNode *start) {
	if( start == NULL )
		return 1;
	return 0;
}

void traverse(DNode *start) {
	DNode *temp = start;
	if(listIsEmpty(start)) {
		printf("\n\nList is empty.\n\n");
		return;
	}
	while(temp != NULL) {
		printf("%d, ", temp->data);
		temp = temp->next;
	}
}

int countNodes(DNode *start) {
	int count = 0;
	DNode *temp = start;
	while(temp != NULL) {
		// printf("%d, ", temp->data);
		temp = temp->next;
		count++;
	}
	return count;
}

char insertAtPosition(DNode **start, int data, int position) {
	DNode *temp, *new_node, *save;
	new_node = createNode(data);
	if(new_node == NULL)
		return 0;
	if(position == 1) {
		new_node->next = *start;
		*start = new_node;
		return 1;
	}
	if(position > countNodes(*start) + 1 || position == 0) {
		fprintf(stderr, "\nError : Invalid position given.\n");
		return 0;
	}
	temp = *start;
	// Traverse till the position
	for(; position > 2 ; position-- )
		temp = temp->next;

	save = temp->next;
	temp->next = new_node;
	new_node->prev = temp;
	new_node->next = save;
	if(save != NULL)
		save->prev = new_node;

	return 1;
}

char deleteAtPosition(DNode **start, int position) {
	DNode *temp, *save;
	temp = *start;
	if(*start == NULL) {
		fprintf(stderr, "\nError : Nothing to delete\n");
		return 0;
	}
	if(position == 1) {
		*start = temp->next;
		if(*start != NULL)
			(*start)->prev = NULL;
		return 1;
	}
	if(position > countNodes(*start) || position == 0) {
		fprintf(stderr, "\nError : Invalid position given.\n");
		return 0;
	}
	// Traverse till the position
	for(; position > 1 ; position-- ){
		save = temp;
		temp = temp->next;
	}
	save->next = temp->next;
	if(temp->next != NULL) {
		temp->next->prev = save;
	}

	free(temp);
	return 1;
}

#endif

// EOF
