/*******************
* FILE : d_linked_list.C
* Author : Shaswata
* Date : 18/10/2015
********************
* Problem : Implement Doubly Linked List
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "d_linked_list.h"

/* MAIN FUNCTION*/
int main() {
	char choice;
  DNode *start = NULL;
  int data, position;
  do {
    printf("\nChoose Operation :\n**********************************************");
    printf("\n1. Traverse Linked List.\n2. Insert Node");
    printf("\n3. Delete Node\n0. Exit Program.\n\n");
    scanf(" %c", &choice);
    printf("\n\n");
    switch(choice) {
      case '1' :
        // Traverse List
        printf("Traversing list : \n");
        traverse(start);
        break;
      case '2' :
        // Insert node at position
				printf("\nEnter Data to store in node : ");
        scanf("%d", &data);
				printf("\nEnter Position to store node : ");
        scanf("%d", &position);
        if(insertAtPosition(&start, data, position))
          printf("\nNode created successfully.\n\n");
        else
          printf("\nNode creation was unsuccessful.\n\n");
        break;
      case '3' :
        // Delete node at
				printf("\nEnter Position to delete node : ");
				scanf("%d", &position);
				printf("\nDeleting Node from position %d...\n", position);
				if(deleteAtPosition(&start, position))
					printf("\nNode deleted successfully.\n\n");
				else
					printf("\nNode deletion was unsuccessful.\n\n");
        break;
      case '0' :
        // Exit program
        printf("\n\n***** THANK YOU *****\n\n");
        return EXIT_SUCCESS;
        break;
      default :
        printf("\n\aWRONG CHOICE! TRY AGAIN\n");
        break;
    }
  }while(1);
  return EXIT_SUCCESS;
}


// EOF
