/*******************
* FILE : evaluation.c
* Author : Shaswata
* Date : 11/11/2015
********************
* Problem : Implement Insertion Sort
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "exp_eval.h"

#define BUFFER_SIZE 20 // Size of buffer to store expression

/* MAIN FUNCTION*/
int main() {
	char infix[BUFFER_SIZE], postfix[BUFFER_SIZE];
	printf("\nEnter Infix Expression : ");
	fgets(infix, sizeof(infix), stdin);
	if( toPostfix(infix, postfix, BUFFER_SIZE) == 0 ) {
			printf("\nInvalid expression. Program terminated.");
			return EXIT_FAILURE;
	}
	printf("\n\nPostfix Exp : %s\n", postfix);
	printf("\n\nResult : %f\n", evalPostfix(postfix));
	return EXIT_SUCCESS;
}
