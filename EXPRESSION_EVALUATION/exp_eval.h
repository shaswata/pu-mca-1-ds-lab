/*******************
* FILE : exp_eval.h
* Author : Shaswata
* Date : 11/11/2015
********************
* Problem : Implement infix to postfix and evaluate postfix
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../STACK_ARRAY/stack.h"

/** FUNCTION isOperator
* 	@params
*		symbol : symbol to be checked
*		@return : 1 is symbol is operator, otherwise 0
*/
char isOperator(char symbol) {
  switch(symbol) {
    case '+':
    case '-':
    case '*':
    case '/':
    case '^':
      return 1;
    default:
      return 0;
  }
}

/** FUNCTION getPrecedence
* 	@params
*		operator : operator whose precedence needs to be found
*		@return : precedence of the operator. 0 if not an operator
*/
short getPrecedence(char operator) {
  switch(operator) {
    case '^' : return 3;
    case '*':
    case '/' : return 2;
    case '+':
    case '-' : return 1;
  }
  return 0;
}

/** FUNCTION toPostfix
* 	@params
*		infix : string with infix notation
*   postfix : buffer to store postfix notation. Required memory should be allocated
*   buffer_size : size of the buffer used for postfix.
*		@return : void
*/
char toPostfix(char infix[], char postfix[], int buffer_size) {
  int infix_length, position, postPos, preStack, preOptr;
  /*
    infix_length : Hold length of infix expression
    position : Loop control
    preStack : precedence of operator in TOP of stack
    preOptr : precedence of operator under operation
  */
  char symbol;
  STACK *optr_stack = createStack(buffer_size);
  infix_length = strlen(infix);
  postPos = 0;
  PUSH(optr_stack, '(');
  for(position = 0; position < infix_length-1; position++) {
    symbol = infix[position];
    if(isOperator(symbol)) {
      preStack = getPrecedence(getTOP(optr_stack));
      preOptr = getPrecedence(symbol);
      while(preStack >= preOptr) {
        postfix[postPos++] = POP(optr_stack);
        preStack = getPrecedence(getTOP(optr_stack));
      }
      PUSH(optr_stack, symbol);
    } else if(symbol == ')') {
      while(getTOP(optr_stack) != '(') {
        symbol = POP(optr_stack);
        postfix[postPos++] = symbol;
      }
    } else if(symbol == '(') {
      PUSH(optr_stack, symbol);
    } else if(symbol == ' ' || symbol == '\t') {
      continue;
    } else {
      postfix[postPos++] = symbol;
    }
  }
  while(getTOP(optr_stack) != '(') {
    symbol = POP(optr_stack);
    postfix[postPos++] = symbol;
  }
  postfix[postPos] = '\0';
  free(optr_stack);
  return 1;
}

/** FUNCTION evalPostfix
* 	@params
*		expression : expression to evaluate
*		@return : result after evaluation
*/
float evalPostfix(char expression[]) {
  int position, length;
  STACK *stack;
  float operand1, operand2, result = 0.0;
  char symbol;
  length = strlen(expression);
  stack = createStack(length);
  for( position = 0; position < length; position++ ) {
    symbol = expression[position];
    if(isOperator(symbol)) {
      operand1 = POP(stack);
      operand2 = POP(stack);
      switch(symbol) {
        case '+' :
          result = operand1 + operand2;
          break;
        case '-' :
          result = operand1 - operand2;
          break;
        case '*' :
          result = operand1 * operand2;
          break;
        case '/' :
          result = operand2 / operand1;
          break;
        case '^' :
          result = pow(operand1, operand2);
          break;
        default :
          printf("\n\nIllegal Operation. Exitting program.!\n\n");
          exit(EXIT_FAILURE);
      }
      PUSH(stack, result);
    } else {
      PUSH(stack, symbol - '0');
    }
  }
  return result;
}
// EOF
