/*******************
* FILE : binary_search.C
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement binary searching technique
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "binary_search.h"
#include "../DISPLAY/print_array.h"
#include "../BUBBLE_SORT/bubble_sort.h"
// #include <conio.h>

/*** GLOBAL DECLARATIONS ***/
static int length; // Store Length of array under operation

/* MAIN FUNCTION*/
int main() {
	int *array, i = 0, key, result;
	// clrscr();
	printf("Enter Required Lenght for the array : ");
	scanf("%d", &length);

	// Allocate memory for array
	array = (int *) malloc(sizeof(int) * length);
	if(array == NULL) {
		printf("\nError : Memory Not Allocated!\n");
		// exit(1);
		return EXIT_FAILURE;
	}
	for(; i < length; i++ ) {
		printf("\nEnter element %d of %d : ", i+1, length);
		scanf("%d", &array[i]);
	}
	bubbleSort(array, length, 'N');
	printArray(array, length);
	printf("\nEnter Search Key : \n");
	scanf("%d", &key);
	result = binarySearch(array, key, 0, length-1);
	if(result>=0)
		printf("KEY : %d IS FOUND AT INDEX %d.\n", key, result);
	else
		printf("KEY : %d IS NOT FOUND.\n", key);
	// getch();
	return EXIT_SUCCESS;
}
