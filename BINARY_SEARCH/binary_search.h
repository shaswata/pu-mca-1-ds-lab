/*******************
* FILE : binary_search.h
* Author : Shaswata
* Date : 30/10/2015
********************
* Problem : Implement binary searching technique
********************/

#ifndef BINARY_SEARCH
#define BINARY_SEARCH

/** FUNCTION BINARY_SEARCH
* 	@params
*		array : pointer to main array
*		key : search key
*		min : location of lower index
*		max : location of upper index
*/
int binarySearch(int *array, int key, int min, int max) {
  int mid, a;
	mid = (max + min) / 2;
	// printf("MIN : %d ; MID : %d ; MAX : %d\n", min, mid, max); // DEBUG
	if(min > max)
		return -1; // Termination condition for false output
	// scanf("%d", &a);
	if(key == array[mid])
		return mid; // Terminating condition for true output
	else if(key > array[mid])
		return binarySearch(array, key, mid+1, max);
	else if(key < array[mid])
		return binarySearch(array, key, min, mid-1);
}

#endif
