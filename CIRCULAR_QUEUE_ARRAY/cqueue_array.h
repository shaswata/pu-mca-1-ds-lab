/*******************
* FILE : queue.c
* Author : Shaswata
* Date : 16/11/2015
********************
* Problem : Implement queue using array
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>

typedef struct queue {
  int *queue;
  int FRONT;
  int REAR;
  int SIZE;
} QUEUE;

/** FUNCTION CREATE QUEUE
* 	@params
*		*queue : size of the queue to be created
*
*   @return : pointer to newly created queue. pointer will be null if memory
*             was not allocated
*/
QUEUE * createQueue(int size) {
  QUEUE *new_queue = (QUEUE *) malloc(sizeof(QUEUE));
  new_queue->queue = (int *) malloc(sizeof(int) * size);
  new_queue->FRONT = new_queue->REAR = -1;
  new_queue->SIZE = size - 1;
  return new_queue;
}

/** FUNCTION ENQUEUE
* 	@params
*   *queue : Queue to be operated upon
*		data : data to be enqueued
*
*   @return : 1 on successful enque, otherwise 0
*/
char enqueue(QUEUE *queue, int data) {
  if( ( queue->FRONT == -1 && queue->REAR == queue->SIZE ) || ( queue->REAR == queue->FRONT && queue->FRONT != -1 ) )
    return 0;

  if( queue->FRONT == -1 )
    queue->REAR++;
  else if( queue->REAR == queue->SIZE )
    queue->REAR = 0;
  else
    queue->REAR++;

  queue->queue[queue->REAR] = data;
  return 1;
}

/** FUNCTION DEQUEUE
* 	@params
*		*queue : queue to be operated upon
*
*   @return : -1 if dequeue is unsuccessful otherwise dequeued data
*/
int dequeue(QUEUE *queue) {
  int front;

  if( queue->REAR == -1 )
    return -1;

  if( queue->FRONT == queue->SIZE )
    queue->FRONT = -1;

  front = queue->queue[++queue->FRONT];

  if( queue->FRONT == queue->REAR )
    queue->FRONT = queue->REAR = -1;

  return front;
}

/** FUNCTION GET FRONT
* 	@params
*		*queue : Queue to be operated upon
*
*   @return : pointer to newly created stack. pointer will be null if memory
*             was not allocated
*/
int getFront(QUEUE *queue) {
  if( queue->REAR == -1 )
    return -1;

  if( queue->FRONT < queue->SIZE )
    return queue->queue[queue->FRONT+1];
  else
    return queue->queue[0];
}

// EOF
