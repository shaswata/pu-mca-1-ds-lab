/*******************
* FILE : queue.c
* Author : Shaswata
* Date : 16/11/2015
********************
* Problem : Implement queue using array
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "cqueue_array.h"

/* MAIN FUNCTION*/
int main() {
  QUEUE *queue = NULL;
  int size, front, element;
  char choice;
  printf("\nEnter size of Queue : ");
  scanf("%d", &size);
  if(size < 1) { // Invalid size
    fprintf(stderr, "Error : Invalid Size. Program terminated.\n");
    return EXIT_FAILURE;
  }
  // Create new queue
  queue = createQueue(size);
  if(queue->queue == NULL) {
    fprintf(stderr, "Error : Memory could not be allocated for new queue. Try again!\n");
    return EXIT_FAILURE;
  }
  do {
    printf("\nChoose Operation :\n**********************************************");
    printf("\n1. ENQUEUE.\n2. DEQUEUE.\n3. Display FRONT.\n4. Exit Program.\n\n");
    scanf(" %c", &choice);
    printf("\n\n");
    switch(choice) {
      case '1':
        // Enqueue data in queue
        printf("\n\nEnter value to ENQUEUE : ");
        scanf("%d", &element);
        if(enqueue(queue, element))
          printf("\nValue successfully ENQUEUED in queue.");
        else
          fprintf(stderr, "\nError : QUEUE FULL EXCEPTION OCCURED!!\n\n");
        break;
      case '2':
        // DEQUEUE data in queue
        front = dequeue(queue);
        if(front != -1)
          printf("\nValue DEQUEUED from queue : %d", front);
        else
          fprintf(stderr, "\nError : QUEUE EMPTY EXCEPTION OCCURED!!\n\n");
        break;
      case '3':
        // Show FRONT element of queue
        front = getFront(queue);
        if(front == -1)
          printf("\nQUEUE IS EMPTY.\n");
        else
          printf("\nFRONT : %d", front);
        break;
      case '4':
        // Exit program
        printf("\n\n***** THANK YOU *****\n\n");
        return EXIT_SUCCESS;
        break;
      default:
        printf("\n\aUNKNOWN INPUT! TRY AGAIN\n");
        break;
    }
  }while(1);
  return EXIT_SUCCESS;
}

// EOF
