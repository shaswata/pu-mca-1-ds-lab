/*******************
* FILE : graph.c
* Author : Shaswata
* Date : 02/11/2015
********************
* Problem : Implement Graph representation using adjacency matrix and perform
*           traversals using DFS and BFS
********************/

#include <stdio.h>

int adjacency_matrix[20][20],q[20],visited[20],n,i,j,f=0,r=-1;

void bfs(int v) {
  for( i=1; i<=n; i++ )
    if( adjacency_matrix[v][i] && !visited[i] )
      q[++r] = i;
  if( f <= r ) {
    visited[ q[f] ]=1;
    bfs( q[f++] );
  }
}

void dfs(int i) {
    int j;
    visited[i] = 1;
    for( j = 0; j < n; j++)
       if( !visited[j] && adjacency_matrix[i][j] == 1 )
            dfs(j);
}

void main() {
  int v;
  printf("\nEnter the number of vertices:");
  scanf("%d", &n);
  for( i = 1; i <= n; i++ ) {
    q[i] = 0;
    visited[i] = 0;
  }
  printf("\nEnter the adjacency matrix:\n");

  for( i = 1; i <= n; i++ )
    for( j = 1; j <= n; j++ )
      scanf("%d", &adjacency_matrix[i][j]);

  printf("\nEnter the starting vertex:");
  scanf("%d", &v);

  bfs(v);
  printf("\nThe node which are reachable using BFS are :\n");
  for( i = 1; i <= n; i++)
    if( visited[i] )
      printf("%d\t", i);
    else
      printf("\n BFS is not possible.");

  printf("\n\n");

  for( i = 1; i <= n; i++ )
    visited[i] = 0;

  printf("\nThe node which are reachable using DFS are :\n");
  dfs(v);
  for( i = 1; i <= n; i++ )
    if( visited[i] )
      printf("%d\t", i);
    else
      printf("\nDFS is not possible on node : %d.", i);
  printf("\n\n");
}
