/*******************
* FILE : pqueue.h
* Author : Shaswata
* Date : 16/11/2015
********************
* Problem : Implement priority queue
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>

int count_node = 0;

struct node {
  int priority;
  int processId;
  struct node *next;
}*head=NULL,*curr=NULL;

void insert() {
  struct node *temp;
  if(head == NULL) {
    head = (struct node *)malloc(sizeof(struct node));
    printf("Enter the process id : ");
    scanf("%d",&head->processId);
    printf("Enter the priority : ");
    scanf("%d",&head->priority);
    head->next = NULL;
    curr = head;
    count_node = 1;
  } else {
    temp = (struct node *)malloc(sizeof(struct node));
    printf("Enter the process id : ");
    scanf("%d",&temp->processId);
    printf("Enter the priority : ");
    scanf("%d",&temp->priority);
    temp->next = NULL;
    curr->next = temp;
    curr = temp;
    count_node += 1;
  }
  printf("\nThe process is inserted.\n");
}

void delete() {
  struct node *temp = head,*prev;
  int data, prio = head->priority;

  while(temp != NULL) {
    if(temp->priority > prio)
    prio = temp->priority;
    temp = temp->next;
  }
  temp = head;
  while(temp->next != NULL) {
    if(temp->priority == prio) {
      if(temp == head) {
        head = temp->next;
        data = temp->processId;
        free(temp);
        break;
      }
      prev->next = temp->next;
      data = temp->processId;
      free(temp);
      break;
    }
    prev = temp;
    temp = temp->next;
  }
  printf("\nPID %d process is deleted.\n",data);
}

void display() {
  struct node *temp = head;
  while(temp != NULL) {
    printf("PID : %d\t",temp->processId);
    printf("PRIORITY : %d\n",temp->priority);
    temp = temp->next;
  }
}
// EOF
