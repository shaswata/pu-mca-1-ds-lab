/*******************
* FILE : pqueue.c
* Author : Shaswata
* Date : 16/11/2015
********************
* Problem : Implement priority queue
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
#include <stdlib.h>
#include "pqueue.h"

/* MAIN FUNCTION*/
int main() {
  int choice;
  while(1) {
    printf("****************************************");
    printf("\n1. Insert the process : \n");
    printf("2. Delete the process : \n");
    printf("3. Display all processes : \n");
    printf("4. Exit : \n");
    printf("****************************************");
    printf("\nEnter your choice = ");
    scanf("%d",&choice);
    switch(choice) {
      case 1:
        insert();
        break;
      case 2:
        delete();
        break;
      case 3:
        display();
        break;
      case 4:
        return EXIT_SUCCESS;
        default:
      printf("Invalid choice ...");
    }
  }
}
// EOF
