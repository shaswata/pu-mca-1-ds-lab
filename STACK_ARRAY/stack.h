/*******************
* FILE : stack.h
* Author : Shaswata
* Date : 07/11/2015
********************
* Problem : Implement stack using array
********************/

/*** ALL INCLUDES HERE ***/
#include <stdio.h>
// #include <conio.h>

typedef struct stack {
  int *stack; // Pointer to stack array
  int TOP; // TOP Index
  int MAX; // Max index of the stack
} STACK;

/** FUNCTION CREATE STACK
* 	@params
*		size : size of the stack to be created
*
*   @return : pointer to newly created stack. pointer will be null if memory
*             was not allocated
*/
STACK * createStack(int size) {
  STACK *new_stack = (STACK *) malloc(sizeof(STACK));
  new_stack->stack = (int *) malloc(size * sizeof(int)); // Allocate memory for array
  new_stack->TOP = -1; // Initialize TOP pointer to EMPTY STACK condition
  new_stack->MAX = size - 1;
  return new_stack;
}

/** FUNCTION SELECTION_SORT
* 	@params
*		STACK *stack : Stack whose TOP element is requested
*
*   @return Element pointed by the TOP pointer. If stack is empty then -1
*/
int getTOP(STACK *stack) {
  return (stack->TOP != -1)? stack->stack[stack->TOP] : -1;
}

/** FUNCTION SELECTION_SORT
* 	@params
*		*stack : stack on which element to be pushed
*		element : value of element to be pushed
*
*   @return : 0 if STACK OVERFLOW, otherwise 1
*/
char PUSH(STACK *stack, int element) {
  if(stack->TOP == stack->MAX)
    return 0;
  stack->TOP++; // Increment stack pointer
  stack->stack[stack->TOP] = element;
  return 1;
}

/** FUNCTION SELECTION_SORT
* 	@params
*		*stack : stack on which element to be pushed
*
*   @return : 0 if STACK UNDERFLOW, otherwise the element popped
*/
int POP(STACK *stack) {
  if(stack->TOP == -1)
    return -1;
  return stack->stack[stack->TOP--]; // Decrement stack pointer and return popped value
}
// EOF
