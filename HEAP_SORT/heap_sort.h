/*******************
* FILE : heap_sort.h
* Author : Shaswata
* Date : 19/11/2015
********************
* Problem : Implement Heap Sort
********************/

#include "../DISPLAY/print_array.h"


#ifndef HEAP_SORT
#define HEAP_SORT

struct MaxHeap
{
    int size; // current size
    int* array; // array of elements
};

/** FUNCTION SWAP
* 	@params
*		*a : pointer to one variable to swap
*   *b : pointer to second variable to swap
*/
void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

/** FUNCTION maxHeapify
*/
void maxHeapify(struct MaxHeap* maxHeap, int idx) {
    int largest = idx;  // Initialize largest as root
    int left = (idx << 1) + 1;  // left = 2*idx + 1
    int right = (idx + 1) << 1; // right = 2*idx + 2

    if (left < maxHeap->size &&
        maxHeap->array[left] > maxHeap->array[largest])
        largest = left;

    if (right < maxHeap->size &&
        maxHeap->array[right] > maxHeap->array[largest])
        largest = right;

    if (largest != idx) {
        swap(&maxHeap->array[largest], &maxHeap->array[idx]);
        maxHeapify(maxHeap, largest);
    }
}

/** FUNCTION maxHeapify
*  Function to create a max heap of given size
*/
struct MaxHeap* createAndBuildHeap(int *array, int size) {
    int i;
    struct MaxHeap* maxHeap =
              (struct MaxHeap*) malloc(sizeof(struct MaxHeap));
    maxHeap->size = size;   // initialize size of heap
    maxHeap->array = array; // Assign address of first element of array

    // Start from bottommost and rightmost internal mode and heapify all
    // internal modes in bottom up way
    for (i = (maxHeap->size - 2) / 2; i >= 0; --i)
        maxHeapify(maxHeap, i);
    return maxHeap;
}


/** FUNCTION heapSort
*/
void heapSort(int* array, int size) {
    struct MaxHeap* maxHeap = createAndBuildHeap(array, size);

    while (maxHeap->size > 1) {
        swap(&maxHeap->array[0], &maxHeap->array[maxHeap->size - 1]);
        --maxHeap->size;  // Reduce heap size

        maxHeapify(maxHeap, 0);
    }
}

#endif

// EOF

/**
Call the buildMaxHeap() function on the list. Also referred to as heapify(), this builds a heap from a list in O(n) operations.
Swap the first element of the list with the final element. Decrease the considered range of the list by one.
Call the siftDown() function on the list to sift the new first element to its appropriate index in the heap.
Go to step (2) unless the considered range of the list is one element.
*//*


function heapSort(a, count) is
   input: an unordered array a of length count

   (first place a in max-heap order)
   heapify(a, count)

   end := count - 1
   while end > 0 do
      (swap the root(maximum value) of the heap with the
       last element of the heap)
      swap(a[end], a[0])
      (decrement the size of the heap so that the previous
       max value will stay in its proper place)
      end := end - 1
      (put the heap back in max-heap order)
      siftDown(a, 0, end)

function heapify(a,count) is
   (start is assigned the index in a of the last parent node)
   start := (count - 2) / 2

   while start ≥ 0 do
      (sift down the node at index start to the proper place
       such that all nodes below the start index are in heap
       order)
      siftDown(a, start, count-1)
      start := start - 1
   (after sifting down the root all nodes/elements are in heap order)

function siftDown(a, start, end) is
   (end represents the limit of how far down the heap to sift)
   root := start

   while root * 2 + 1 ≤ end do       (While the root has at least one child)
      child := root * 2 + 1           (root*2+1 points to the left child)
      (If the child has a sibling and the child's value is less than its sibling's...)
      if child + 1 ≤ end and a[child] < a[child + 1] then
         child := child + 1           (... then point to the right child instead)
      if a[root] < a[child] then     (out of max-heap order)
         swap(a[root], a[child])
         root := child                (repeat to continue sifting down the child now)
      else
         return






*/
