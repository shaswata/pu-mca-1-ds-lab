#include <stdio.h>
#include <stdlib.h>

struct Hash {
 int info;
 struct Hash *next;
};

void search(struct Hash *key,int n);
void display(struct Hash *key,int n);
void insert(struct Hash **key,int n);

void main() {
	int n,i,choice;
	struct Hash *key;

	printf("Enter no of key you want to create:\t");
	scanf("%d",&n);

	key=(struct Hash *)calloc(n,sizeof(struct Hash));

	for(i=0;i<n;i++) {
  	(key+i)->info=i;
  	(key+i)->next=NULL;
	}

	display(key,n);

	while(1) {
		printf("\nOPTIONS\n");
		printf("1.Insert\n");
		printf("2.Search\n");
		printf("3.Exit\n");
		printf("Enter choice:\t");
		scanf("%d",&choice);
		switch(choice) {
		 case 1:
			 insert(&key,n);
			 display(key,n);
			 break;

		 case 2:
			 display(key,n);
			 search(key,n);
			 break;

		 case 3: exit(0);
			 break;

		 default: printf("Enter correct option!!!");
		}
	}
}

void insert(struct Hash **key,int n) {
 struct Hash *temp;
 int formulae,element;

  temp=(struct Hash *)calloc(1,sizeof(struct Hash));
  printf("Enter the element:\t");
  scanf("%d",&element);
  temp->info=element;
  temp->next=NULL;
  formulae = element % n;

  if ((*key+formulae)->next==NULL)
	   (*key+formulae)->next=temp;
  else
	 temp->next=(*key+formulae)->next;
	  (*key+formulae)->next=temp;
}

void display(struct Hash *key,int n) {
 int i;
 struct Hash *temp;

 printf("Key\tElements");

	for(i=0;i<n;i++) {
	temp=(key+i);
	printf("\n");
		while(temp!=NULL) {
  		printf("%d\t",(temp)->info);
  		temp=temp->next;
		}
	}
}

void search(struct Hash *key,int n) {
 int search_element,key_index,column=1,i=0;
 struct Hash *temp;

 printf("\nEnter the element to search:\t");
 scanf("%d",&search_element);

 key_index=search_element%n;
 temp=(key+key_index)->next;

	while(temp!=NULL) {
		if (temp->info==search_element) {
      printf("[%d] is present at [%d] row and [%d] coloum.",search_element,key_index,column);
  		i=1;
		}
  	temp=temp->next;
  	column=column+1;
	}

	if(i==0)
		printf("[%d] is not present in Hash table.",search_element);
}
