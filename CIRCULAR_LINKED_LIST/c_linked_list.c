/*******************
* FILE : c_linked_list.c
* Author : Shaswata
* Date : 18/11/2015
********************
* Problem : Implement Doubly Linked List
********************/

#include <stdio.h>
#include <stdlib.h>
#include "c_linked_list.h"

/* MAIN FUNCTION */
int main() {
	int ch;
	printf("\n1.Creation\n2.Insertion at beginning\n3.Insertion at remaining");
	printf("\n4.Deletion at beginning \n5.Deletion at position \n6.traverse");
	printf("\n0.Exit\n");
	while (1) {
		printf("\nEnter your choice:");
		scanf("%d", &ch);
		switch(ch) {
			case 1:
				create();
				break;
			case 2:
				ins_at_beg();
				break;
			case 3:
				ins_at_pos();
				break;
			case 4:
				del_at_beg();
				break;
			case 5:
				del_at_pos();
				break;
			case 6:
				traverse();
				break;
			case 0:
				return EXIT_SUCCESS;
			default:
				printf("\nWrong Choice!! Try Again!!\n");
		}
	}
}
