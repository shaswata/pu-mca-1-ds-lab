/*******************
* FILE : shortest_path.c
* Author : Shaswata
* Date : 02/11/2015
********************
* Problem : Implement Graph representation using adjacency matrix and perform
*           traversals using DFS and BFS
********************/

#include <stdio.h>
#include <stdlib.h>

int main(){
  int path[5][5],i,j,min,a[5][5],p,st=1,ed=5,stp,edp,t[5],index;

  printf("\nEnter the cost matrix : \n");

  for(i=1;i<=5;i++)
    for(j=1;j<=5;j++)
      scanf("%d",&a[i][j]);

  printf("\nEnter  number of paths : \n");
  scanf("%d",&p);
  printf("\nEnter possible paths : \n");

  for(i=1;i<=p;i++)
    for(j=1;j<=5;j++)
      scanf("%d",&path[i][j]);

  for(i=1;i<=p;i++) {
    t[i]=0;
    stp=st;
    for(j=1;j<=5;j++) {
      edp=path[i][j+1];
      t[i]=t[i]+a[stp][edp];
      if(edp==ed)
        break;
      else
        stp=edp;
      }
    }

    min=t[st];index=st;

    for(i=1;i<=p;i++) {
      if(min>t[i]) {
        min=t[i];
        index=i;
      }
    }

    printf("minimum cost %d",min);
    printf("\n minimum cost path ");

    for(i=0;i<5;i++) {
      printf("--> %d",path[index][i]);
      if(path[index][i]==ed)
        break;
    }
    printf("\n");
    return EXIT_SUCCESS;
}
